# PRACTICE TWO

- Overview

  - Design: <a href="https://www.figma.com/file/dM4fuTbvN1JNbJlB0DTeS8/main-(Copy)?node-id=0%3A1">MODERNO</a>

  - Timeline: 1 week

- Requirements

  - Use the right HTML Tags

  - Apply Flexbox and Grid

  - Use validate tools: <a href="https://validator.w3.org/" >W3 Validator</a>

  - Getting the code to work cross browsers latest version (Chrome, Firefox, MS Edge, Opera, Safari) (\*)

- Targets

  - Improve more ability picking HTML tags and styling solutions

  - More practice with CSS selectors

  - Learn how to make a responsive website

  - Learn how to make a website that can cross-browse

  **NGUYEN HUU DU**
